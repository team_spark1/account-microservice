package com.synechron.accountmicroservice.controller;

import java.util.Map;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.synechron.accountmicroservice.model.Account;
import com.synechron.accountmicroservice.service.AccountService;

@RestController
@RequestMapping("/api/accounts")
public class AccountController {
	
	private final AccountService accountService;
	
	public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, Object> fetchAll(
            @RequestParam(name = "page", required = false, defaultValue = "0") int page,
            @RequestParam(name = "size", required = false, defaultValue = "10") int size,
            @RequestParam(name = "attr", required = false, defaultValue = "accountId") String property
    ) {
        return this.accountService.fetchAccountStatement(page, size, property);
    }
	
	@GetMapping(value = {"/{accountId}" }, produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, Object> fetchAccountByAccountId(@PathVariable("accountId") long accountId) {
		return this.accountService.fetchAccountById(accountId);
    }
	
	@GetMapping(value = {"/{accountId}/statement"}, produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, Object> fetchStatementByAccountId(@PathVariable("accountId") long accountId,
    		@RequestParam(name = "start_date", required = false) String start,
            @RequestParam(name = "end_date", required = false) String end) {
		return this.accountService.getAccountStatement(accountId,start,end);
    }
	
	@PutMapping("/{id}")
    public Account updateAmountByAccId(@PathVariable("id") long accountId, @RequestBody Account account) {
        return this.accountService.updateAmount(accountId, account);
    }
}