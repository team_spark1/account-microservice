package com.synechron.accountmicroservice.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "accounts")
@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class Account {
	
	@Id
	@Column(name = "cust_id")
    private Long accountId;
	
	@JsonProperty(value = "customer", required = true)
    @OneToOne
    @MapsId
    @JoinColumn(name = "cust_id")
    private Customer customer;
	
	@Transient
	private String trnscType;
	
	@Transient
	private String amountBy;
	
	@OneToMany(mappedBy = "account", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<AccountEntries> accountEntries = new HashSet<>();
	
    @JsonProperty(value = "amount_field", required = true)
	private BigDecimal amount;
    
    private Integer cibilScore;
	
	@JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonProperty(value = "date", required = true)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private LocalDate date;
	
	public void setAccountEntries(Set<AccountEntries> accountEntries) {
		accountEntries.forEach(accountEntry -> accountEntry.setAccount(this));
        this.accountEntries = accountEntries;
    }

    public void addAccountEntry(AccountEntries accountEntry) {
        if (this.accountEntries == null) {
            this.accountEntries = new HashSet<>();
        }
        this.accountEntries.add(accountEntry);
        accountEntry.setAccount(this);
    }
}
