package com.synechron.accountmicroservice.model;

public enum Type {

	CREDIT,
	DEBIT
}
