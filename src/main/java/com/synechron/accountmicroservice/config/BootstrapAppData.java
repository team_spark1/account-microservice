package com.synechron.accountmicroservice.config;

import static java.util.stream.IntStream.range;

import java.math.BigDecimal;
import java.time.ZoneId;
import java.util.concurrent.TimeUnit;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import com.github.javafaker.Faker;
import com.synechron.accountmicroservice.model.Account;
import com.synechron.accountmicroservice.model.AccountEntries;
import com.synechron.accountmicroservice.model.Customer;
import com.synechron.accountmicroservice.repository.CustomerRepository;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class BootstrapAppData implements ApplicationListener<ApplicationReadyEvent> {

    private final CustomerRepository customerRepository;
    private final Faker faker = new Faker();


    public void onApplicationEvent(ApplicationReadyEvent event) {

    	BigDecimal finalAmount = new BigDecimal(0);
    	for(int index = 0; index < 100; index ++){
    		finalAmount = new BigDecimal(0);
        Customer customer = Customer
                .builder()
                .id(faker.number().numberBetween(1000000L, 2000000L))
                .name(faker.name().fullName())
                .email(faker.name().firstName() + "@gmail.com")
                .phoneNumber("+919632502541")
                .build();
        Account account = Account
                .builder()
                .customer(customer)
                .cibilScore(faker.number().numberBetween(300, 900))
                .build();
        customer.setAccount(account);
        range(0, 4).forEach(value -> {
            AccountEntries entry = AccountEntries
                    .builder()
                    .trnscType("Credit")
                    .amountBy(faker.name().firstName())
                    .amount(BigDecimal.valueOf(faker.number().randomDouble(2,25000, 3000000)))
                    .date(faker.date().past(5, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
                    .build();

            account.addAccountEntry(entry);
        });
        for (AccountEntries entry : account.getAccountEntries()) {
        	finalAmount = finalAmount.add(entry.getAmount());			
		}
        customer.getAccount().setAmount(finalAmount);;
        //this.customerRepository.save(customer);
    }
    }
}