package com.synechron.accountmicroservice.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.synechron.accountmicroservice.model.Account;
import com.synechron.accountmicroservice.model.AccountEntries;
import com.synechron.accountmicroservice.model.Type;
import com.synechron.accountmicroservice.repository.AccountRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AccountService {
	private final AccountRepository accountRepository;
	
    public AccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }
    
    public Map<String, Object> fetchAccountStatement(int page, int size, String property) {
        PageRequest pageRequest = PageRequest.of(page, size, Sort.by(property));
        final Page<Account> pageResponse = this.accountRepository.findAll(pageRequest);
        final long totalRecords = pageResponse.getTotalElements();
        final int pages = pageResponse.getTotalPages();
        final List<Account> data = pageResponse.getContent();

        Map<String, Object> resultMap = new LinkedHashMap<>();
        resultMap.put("total-records", totalRecords);
        resultMap.put("pages", pages);
        resultMap.put("data", data);
        return resultMap;
    }
    
    public Map<String, Object>  fetchAccountById(long accountId) {
    	Account account =this.accountRepository.findById(accountId).orElseThrow(() -> new IllegalArgumentException("Invalid Account Number"));
        return this.mapAccountDetails(account); 
    }
    
    public Account fetchStatementByAccId(long accountId) {
    	return this.accountRepository.findById(accountId).orElseThrow(() -> new IllegalArgumentException("Invalid Account Number")); 
    }
    
    private Map<String, Object> mapAccountDetails(Account account) {
    	Map<String, Object> accDetailMap = new HashMap<String, Object>();
    	accDetailMap.put("account_id", account.getAccountId());
    	accDetailMap.put("customer", account.getCustomer());
    	accDetailMap.put("balance", account.getAmount());

    	return accDetailMap;
	}
    
    private Map<String, Object> mapStatement(Account account) {
    	Map<String, Object> entriesMap = new HashMap<String, Object>();
    	if(null != account.getAccountEntries()) {
    		entriesMap.put("accountEntries", account.getAccountEntries());
    	}
    	return entriesMap;
	}

	public Account updateAmount(long accountId, Account account) {
    	AccountEntries newEntry = new AccountEntries();
    	Account tempAccount =this.accountRepository.findById(accountId).orElseThrow(() -> new IllegalArgumentException("Invalid Account Number"));
        tempAccount.setAccountId(account.getAccountId());
    	if (Type.CREDIT.toString().equalsIgnoreCase(account.getTrnscType())) {
    		tempAccount.setAmount(tempAccount.getAmount().add(account.getAmount()));
        	newEntry.setTrnscType(Type.CREDIT.toString());
    	} else if (Type.DEBIT.toString().equalsIgnoreCase(account.getTrnscType())) {
    		tempAccount.setAmount(tempAccount.getAmount().subtract(account.getAmount()));
    		newEntry.setTrnscType(Type.DEBIT.toString());
    	}
    	newEntry.setAmount(account.getAmount());
    	newEntry.setAmountBy(account.getAmountBy());
    	newEntry.setDate(account.getDate());
    	tempAccount.addAccountEntry(newEntry);
    	return this.accountRepository.save(tempAccount);
    }

	public Map<String, Object> getAccountStatement(long accountId, String start, String end) {
		Account account = this.fetchStatementByAccId(accountId);
		if(StringUtils.isEmpty(start) || StringUtils.isEmpty(end)){
			return this.mapStatement(account);
		} else {
			return this.mapStatementDate(account, start, end);
		}
	}

	private Map<String, Object> mapStatementDate(Account account, String start, String end) {
		Map<String, Object> entriesMap = new HashMap<String, Object>();
		List<AccountEntries> listEntries = new ArrayList<AccountEntries>();
    	if(null != account.getAccountEntries()) {
    		for (AccountEntries entry : account.getAccountEntries()) {
    			if((entry.getDate().isAfter(LocalDate.parse(start)) 
    					&& entry.getDate().isBefore(LocalDate.parse(end)))
    					|| (entry.getDate().isEqual(LocalDate.parse(start))
    					|| (entry.getDate().isEqual(LocalDate.parse(end))))) {
    				listEntries.add(entry);
    			}
			}
    		entriesMap.put("accountEntries", listEntries);
    	}
    	return entriesMap;
	}
}
